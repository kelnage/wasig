var steam_api = {
    resolveUserId: function(vanityUrl) {
        steam.totalReqCount += 1;
        $.ajax(steam.baseUrl + '/ISteamUser/ResolveVanityURL/v0001/?vanityurl='+encodeURIComponent(vanityUrl),
            {type: "GET", async: false, dataType: 'json'}).done(function(data, status) {
            if(data.response.success == 1) {
                steam.userId = data.response.steamid;
            } else {
                steam.userId = null;
            }
            steam.completedReqs += 1;
        });
    },
    getGameList: function() {
        var $game = $("#game");
        steam.totalReqCount += 1;
        $.getJSON(steam.baseUrl + '/IPlayerService/GetOwnedGames/v0001/?include_played_free_games=1&include_appinfo=1&steamid='+steam.userId, function(data) {
            for(var index in data.response.games) {
                var game = data.response.games[index],
                    icon_url = "http://media.steampowered.com/steamcommunity/public/images/apps/"+game.appid+'/'+game.img_logo_url+'.jpg';
                steam.gameList[game.appid] = {name: game.name, achievements: {}, playTime: game.playtime_forever,
                    icon: icon_url};
                steam_api.getGameDetails(game.appid);
                $game.append('<option value="'+game.appid+'">'+game.name+'</option>');
                steam.isUpdating(true);
            }
            steam.completedReqs += 1;
            $game.selectpicker('refresh')
        });
    },
    getGameDetails: function(gameId) {
        steam.totalReqCount += 1;
        $.getJSON(steam.baseUrl + '/ISteamUserStats/GetSchemaForGame/v2/?appid='+gameId+'&l=english', function(data) {
            if(data.game.availableGameStats) {
                steam_api.getAchievementRarityByGame(gameId);
                steam_api.getPlayerAchievementList(gameId);
                for(var index in data.game.availableGameStats.achievements) {
                    var achievement = data.game.availableGameStats.achievements[index];
                    if(achievement.name in steam.gameList[gameId].achievements) {
                        steam.gameList[gameId].achievements[achievement.name].name = achievement.displayName;
                        steam.gameList[gameId].achievements[achievement.name].icon = achievement.icon;
                        steam.gameList[gameId].achievements[achievement.name].desc = achievement.description;
                    } else {
                        steam.gameList[gameId].achievements[achievement.name] = {
                            name: achievement.displayName,
                            icon: achievement.icon, 
                            desc: achievement.description,
                            percentage: null, got: null
                        };
                    }
                    steam.isUpdating(true);
                }
            }
            steam.completedReqs += 1;
        });
    },
    getAchievementRarityByGame: function(gameId) {
        steam.totalReqCount += 1;
        $.getJSON(steam.baseUrl + '/ISteamUserStats/GetGlobalAchievementPercentagesForApp/v0002/?gameid='+gameId, function(data) {
            for(var index in data.achievementpercentages.achievements) {
                var achievement = data.achievementpercentages.achievements[index];
                if(achievement.percent < 0 || achievement.percent > 100) {
                    achievement.percent = NaN;
                }
                if(achievement.name in steam.gameList[gameId].achievements) {
                    steam.gameList[gameId].achievements[achievement.name].percentage = achievement.percent;
                } else {
                    steam.gameList[gameId].achievements[achievement.name] = {name: null, percentage: achievement.percent, got: null};
                }
                steam.isUpdating(true);
            }
            steam.completedReqs += 1;
        });
    },
    getPlayerAchievementList: function(gameId) {
        steam.totalReqCount += 1;
        $.getJSON(steam.baseUrl + '/ISteamUserStats/GetPlayerAchievements/v0001/?appid='+gameId+'&steamid='+steam.userId, function(data) {
            if(data.playerstats && data.playerstats.achievements) {
                for(var index in data.playerstats.achievements) {
                    var achievement = data.playerstats.achievements[index];
                    if(achievement.apiname in steam.gameList[gameId].achievements) {
                        steam.gameList[gameId].achievements[achievement.apiname].got = achievement.achieved == 1;
                    } else {
                        steam.gameList[gameId].achievements[achievement.apiname] = {name: null, percentage: null, got: achievement.achieved == 1};
                    }
                    steam.isUpdating(true);
                }
            }
            steam.completedReqs += 1;
        });
    }
};
