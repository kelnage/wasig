var steam = {
	baseUrl: '/api/steam',
	gameList: {},
	totalReqCount: 0,
	completedReqs: 0,
	changed: false,
	fetchAchievements: function() {
		var userId = $("#steam_id").val(), $game = $("#game");
		steam.gameList = {};
		$game.empty();
		$game.selectpicker("refresh");
		steam.updateUserId(userId);
		steam.totalReqCount = 0;
		steam.completedReqs = 0;
		steam.$progress.text("Validating Steam user...");
		if(steam.isValidUserId()) {
			steam_api.getGameList();
		}
	},
	displayAchievementsModal: function(game) {
		$("#achievements_title").text("Incomplete achievements for " + game.name);
		var $achievementsTable = $("#achievements_table").children("tbody");
		$achievementsTable.empty();
		game.remainingAchievements.sort(function(a, b) { return b.percentage - a.percentage });
		for(var i = 0; i < game.remainingAchievements.length; i++) {
			var achievement = game.remainingAchievements[i], $row = $("<tr></tr>");
			$row.append('<td><img height="25" width="25" src="'+achievement.icon+'"></td>');
			if(achievement.desc) {
				$row.append('<td><span title="'+achievement.desc+'">'+achievement.name+'</span></td>');
			} else {
				$row.append('<td>'+achievement.name+'</td>');
			}
			$row.append('<td>'+achievement.percentage.toPrecision(2)+'%</td>');
			$achievementsTable.append($row);
		}
		steam.$achievementModal.modal();
	},
	isUpdating: function(changed) {
		steam.changed = changed;
		$updating = $("#updating");
		if(changed && $updating.css("display") == "none") {
			$updating.fadeIn();
		} else if(!changed && $updating.css("display") != "none") {
			$updating.fadeOut();
		}
	},
	updateProgressBar: function() {
		if(steam.totalReqCount !== 0 && steam.totalReqCount === steam.completedReqs) {
			steam.$progress.removeClass('active');
			steam.$progress.attr('aria-valuemax', 100);
			steam.$progress.attr('aria-valuenow', 100);
			steam.$progress.css('width', "100%");
			steam.$progress.text("Completed!");
		}
		else if(steam.totalReqCount !== 0) {
			steam.$progress.addClass('active');
			var percentage = Number(((steam.completedReqs + 1) / (steam.totalReqCount + 1)) * 100).toPrecision(3) + "%";
			steam.$progress.attr('aria-valuemax', steam.totalReqCount);
			steam.$progress.attr('aria-valuenow', steam.completedReqs);
			steam.$progress.css('width', percentage);
			steam.$progress.text(percentage + " complete...");
		} else {
			steam.$progress.removeClass('active');
			steam.$progress.attr('aria-valuemax', 100);
			steam.$progress.attr('aria-valuenow', 100);
			steam.$progress.css('width', "100%");
			steam.$progress.text("Waiting for valid Steam user...");
		}
	},
	updateAchievementLists: function() {
		if(Object.getOwnPropertyNames(steam.gameList).length > 0) {
			var only_played = ($("#only_played").prop("checked") ? 0 : ($("#min_playtime").prop("checked") ? $("#min_playtime_value").val() : -1)),
				todo = steam.getUnachievedAchievements(only_played),
				games = steam.getGameDifficulties(only_played);
			if(todo) {
				todo.sort(steam.orderAchievements(true));
				steam.renderTopAchievements($("#commonest-table"), todo, 10);
				todo.reverse();
				steam.renderTopAchievements($("#rarest-table"), todo, 10);
			}
			if(games) {
				steam.renderGameDifficulty(games, "easy", 10);
				steam.renderGameDifficulty(games, "hard", 10);
				steam.renderTopRemainingAchievements(games, $('#near-comp-table'), true, 10);
				steam.renderTopRemainingAchievements(games, $('#far-comp-table'), false, 10);
			}
		}
		steam.isUpdating(false);
	},
	renderTopRemainingAchievements: function(games, $table, most_complete, max_count) {
		games.sort(function(a, b) { 
			return (most_complete ? a.remainingDifficulty - b.remainingDifficulty : b.remainingDifficulty - a.remainingDifficulty);
		});
		$table.children("tbody").empty();
		for(var i = 0; i < Math.min(max_count, games.length); i++) {
			steam.renderAchievementsRemaining($table, games[i]);
		}
	},
	renderTopAchievements: function($table, todo, max_count) {
		$table.children("tbody").empty();
		for(var i = 0; i < Math.min(max_count, todo.length); i++) {
			steam.renderAchievement($table, todo[i]);
		}
	},
	renderGameDifficulty: function(games, type, count) {
		games.sort(function(a, b) { 
			if(a[type] < b[type]) { 
				return 1;
			} else if(a[type] > b[type]) { 
				return -1;
			} 
			return (steam.compareFields(true, "incomplete"))(a, b); 
		});
		$('#'+type+'-table tbody').empty();
		for(var i = 0; i < Math.min(count, games.length); i++) {
			if(games[i][type] != 0) {
				steam.renderGame($('#'+type+'-table'), games[i], type);
			}
		}
	},
	renderAchievement: function($table, todo) {
		var $row = $("<tr></tr>");
		$row.append('<td><img height="25" width="25" src="'+todo.achievement.icon+'"></td>');
		if(todo.achievement.desc) {
			$row.append('<td><span title="'+todo.achievement.desc+'">'+todo.achievement.name+'</span></td>');
		} else {
			$row.append('<td>'+todo.achievement.name+'</td>');
		}
		$row.append('<td>'+todo.game.name+'</td>');
		$row.append('<td>'+todo.achievement.percentage.toPrecision(2)+'%</td>');
		$table.children("tbody").append($row);
	},
	renderGame: function($table, game, field) {
		var $row = $("<tr></tr>");
		$row.append('<td><img height="25" width="25" src="'+game.icon+'"></td>');
		$row.append('<td>'+game.name+'</td>');
		$row.append('<td>'+game[field]+'/'+game.incomplete+'</td>');
		$row.append('<td>'+steam.formatMinutes(game.playTime)+'</td>');
		$row.click(function() {
			steam.displayAchievementsModal(game);
		});
		$table.children("tbody").append($row);
	},
	renderAchievementsRemaining: function($table, game) {
		var $row = $("<tr></tr>");
		$row.append('<td><img height="25" width="25" src="'+game.icon+'"></td>');
		$row.append('<td>'+game.name+'</td>');
		$row.append('<td>'+game.incomplete+'</td>');
		$row.append('<td>'+(game.percentage * 100).toPrecision(2)+'%</td>');
		$row.click(function() {
			steam.displayAchievementsModal(game);
		});
		$table.children("tbody").append($row);
	},
	isValidUserId: function() {
		if(steam.userId && steam.userId.match("^7656119[0-9]+$")) {
			$("#check_steam_id").hide();
			return true;
		} else {
			$("#check_steam_id").show();
			return false;
		}
	},
	updateUserId: function(potentialUserId) {
		var vanityUrl = potentialUserId.match(/^https?:\/\/steamcommunity\.com\/id\/([^\/]*)\/.*/); 
		if(potentialUserId.match("^7656119[0-9]+$")) {
			steam.userId = potentialUserId;
		} else if(vanityUrl) {
			steam_api.resolveUserId(vanityUrl[1]);
		} else {
			steam_api.resolveUserId(potentialUserId);
		}
	},
	getUnachievedAchievements: function(min_playtime) {
		var results = [];
		for(var i in steam.gameList) {
			var game = steam.gameList[i];
			if(game.name) {
				if(game.playTime > min_playtime) {
					for(var j in game.achievements) {
						var achievement = game.achievements[j];
						if(achievement.name && achievement.got === false && achievement.percentage) {
							results.push({achievement: achievement, game: game});
						}
					}
				}
			}
		}
		return results;
	},
	getGameDifficulties: function(min_playtime) {
		return Object.keys(steam.gameList).filter(function(i) { return steam.gameList[i].playTime > min_playtime; }).map(function(i) {
				var g = steam.gameList[i], achievementKeys = Object.keys(g.achievements),
					unachieved = achievementKeys.filter(function(k) { return g.achievements[k].got === false }),
					achieved = achievementKeys.filter(function(k) { return g.achievements[k].got === true }),
					achievements = unachieved.map(function(k) { return g.achievements[k]; }),
					completed = achieved.length, incomplete = unachieved.length;
				return {
					id: i, name: g.name, icon: g.icon, playTime: g.playTime,
					remainingAchievements: unachieved.map(function(k) { return g.achievements[k] }),
					completed: completed, incomplete: incomplete, percentage: completed / achievementKeys.length,
					remainingDifficulty: unachieved
						.map(function(k) { return 100 / g.achievements[k].percentage })
						.reduce(function(a, b) { return a + b }, 0),
					easy: steam.countAchievements(achievements, function(a) { return a.percentage > 50; }),
					hard: steam.countAchievements(achievements, function(a) { return a.percentage < 10; })
				};
			}).filter(function(a) { return a.incomplete != 0 });
	},
	countAchievements: function(achievements, test) {
		return achievements.map(function(a) { return (test(a) ? 1 : 0); }).reduce(function(a, b) { return a + b; }, 0);
	},
	compareFields: function(invert, field) {
		return function(a, b) {
			if(a[field] < b[field]) {
				return (invert ? 1 : -1);
			} else if(a[field] > b[field]) {
				return (invert ? -1 : 1);
			}
			return 0;
		}
	},
	orderAchievements: function(invert) {
		return function(a, b) {
			return (steam.compareFields(invert, "percentage"))(a.achievement, b.achievement);
		}
	},
	formatMinutes: function(minutes) {
		var n = Number(minutes);
		if(n == 0) {
			return "Unplayed";
		} else if(n < 60) {
			return n + " minute" + (n == 1 ? "" : "s");
		} else if(n < 1440) {
			n = n / 60;
			return n.toPrecision(2) + " hour" + (n == 1 ? "" : "s");
		} else if(n < 10080) {
			n = n / 1440;
			return n.toPrecision(2) + " day" + (n == 1 ? "" : "s")
		} else if(n < 43200) {
			n = n / 10080;
			return n.toPrecision(2) + " week" + (n == 1 ? "" : "s")
		} else if(n < 525600) {
			n = n / 43200;
			return n.toPrecision(2) + " month" + (n == 1 ? "" : "s")
		} else {
			n = n / 525600;
			return n.toPrecision(2) + " year" + (n == 1 ? "" : "s")
		}
	}
}
