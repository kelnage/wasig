var wasig = {
    navItems: {
        "index.html": "Home",
        "steam.html": "Steam"
    },
    buildPage: function(currPage) {
        wasig.buildNav($('nav'), currPage);
        wasig.buildFooter($('footer'));
    },
    buildNav: function($nav, currPage) {
        var $cont = $("<div>").addClass("container").appendTo($nav),
            $header = $("<div>").addClass("navbar-header").appendTo($cont),
            $navbar = $("<div>").attr("id", "navbar").addClass("collapse navbar-collapse").appendTo($cont),
            $items = $("<ul>").addClass("nav navbar-nav").appendTo($navbar),
            $toggle = $("<button>").attr("data-toggle", "collapse").attr("data-target", "#navbar").attr("aria-expanded", "false").attr("aria-controls", "navbar").addClass("navbar-toggle collapsed").appendTo($header),
            $brand = $("<a>").addClass("navbar-brand").attr("attr", "#").text("What Achievement Should I Get?").appendTo($header);
        $nav.addClass("navbar navbar-inverse navbar-fixed-top");
        $toggle.append('<span class="sr-only">Toggle navigation</span>');
        for(var i = 0; i < 3; i++) {
            $toggle.append('<span class="icon-bar"></span>');
        }
        for(var url in wasig.navItems) {
            $items.append('<li'+(currPage == url?' class="active"': '')+'><a href="'+url+'">'+wasig.navItems[url]+'</a></li>');
        }
    },
    buildFooter: function($footer) {
        var $cont = $("<div>").addClass("container").appendTo($footer),
            $text = $("<p>").addClass("text-muted").html('<a href="http://steampowered.com">Powered By Steam</a>! Before using this, you should <em>definitely</em> check out our <a href="privacy.html">privacy policy</a>.').appendTo($cont);
        $footer.addClass("footer");
    }
};
